
import java.io.IOException;

import com.gabfusi.spaguetti.client.controller.*;
import com.gabfusi.spaguetti.client.service.ModelService;

import javafx.fxml.FXMLLoader;

public class AppFactory {
    private ModelService modelService;

    private MainController mainController;
    private LoginController loginController;
    private MatchmakingController matchmakingController;
    private GameController gameController;
    private ResultsController resultsController;

    /**
     *
     * @return
     */
    MainController getMainController() {
        if (mainController == null) {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.load(getClass().getResourceAsStream("com/gabfusi/spaguetti/client/view/Main.fxml"));
                mainController = loader.getController();
                mainController.setModelService(getModelService());

                mainController.setLoginController(getLoginController());
                mainController.setMatchmakingController(getMatchmakingController());
                mainController.setGameController(getGameController());
                mainController.setResultsController(getResultsController());
            } catch (IOException e) {
                throw new RuntimeException("Unable to load Main.fxml", e);
            }
        }
        return mainController;
    }

    /**
     *
     * @return
     */
    private LoginController getLoginController() {
        if (loginController == null) {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.load(getClass().getResourceAsStream("com/gabfusi/spaguetti/client/view/Login.fxml"));
                loginController = loader.getController();
                loginController.setModelService(getModelService());
                loginController.setMainController(getMainController());
                loginController.setModelService(getModelService());

            } catch (IOException e) {
                throw new RuntimeException("Unable to load Login.fxml", e);
            }
        }
        return loginController;
    }

    /**
     *
     * @return
     */
    private MatchmakingController getMatchmakingController() {
        if (matchmakingController == null) {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.load(getClass().getResourceAsStream("com/gabfusi/spaguetti/client/view/Matchmaking.fxml"));
                matchmakingController = loader.getController();
                matchmakingController.setModelService(getModelService());
                matchmakingController.setMainController(getMainController());
                matchmakingController.setModelService(getModelService());

            } catch (IOException e) {
                throw new RuntimeException("Unable to load Matchmaking.fxml", e);
            }
        }
        return matchmakingController;
    }

    /**
     *
     * @return
     */
    private GameController getGameController() {
        if (gameController == null) {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.load(getClass().getResourceAsStream("com/gabfusi/spaguetti/client/view/Game.fxml"));
                gameController = loader.getController();
                gameController.setModelService(getModelService());
                gameController.setMainController(getMainController());
                gameController.setModelService(getModelService());

            } catch (IOException e) {
                throw new RuntimeException("Unable to load Game.fxml", e);
            }
        }
        return gameController;
    }

    /**
     *
     * @return
     */
    private ResultsController getResultsController() {
        if (resultsController == null) {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.load(getClass().getResourceAsStream("com/gabfusi/spaguetti/client/view/Results.fxml"));
                resultsController = loader.getController();
                resultsController.setModelService(getModelService());
                resultsController.setMainController(getMainController());
                resultsController.setModelService(getModelService());

            } catch (IOException e) {
                throw new RuntimeException("Unable to load Results.fxml", e);
            }
        }
        return resultsController;
    }

    /**
     *
     * @return
     */
    private ModelService getModelService() {
        return modelService;
    }

    /**
     *
     * @param modelService
     */
    void setModelService(ModelService modelService) {
        this.modelService = modelService;

        mainController.setModelService(getModelService());
        loginController.setModelService(getModelService());
        matchmakingController.setModelService(getModelService());
        gameController.setModelService(getModelService());
        resultsController.setModelService(getModelService());

        // append listener to controllers
        this.modelService.getMessageReceiver().addListener(getLoginController());
        this.modelService.getMessageReceiver().addListener(getMatchmakingController());
        this.modelService.getMessageReceiver().addListener(getGameController());
        this.modelService.getMessageReceiver().addListener(getResultsController());
    }

}