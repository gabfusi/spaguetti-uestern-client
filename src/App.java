
import com.gabfusi.spaguetti.client.model.Game;
import com.gabfusi.spaguetti.client.networking.MessageReceiver;
import com.gabfusi.spaguetti.client.networking.MessageSender;
import com.gabfusi.spaguetti.client.networking.socket.SocketClient;
import com.gabfusi.spaguetti.client.service.ModelService;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import com.gabfusi.spaguetti.client.controller.MainController;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {

        AppFactory factory = new AppFactory();
        MainController mainController = factory.getMainController();

        // create a scene
        Scene scene = new Scene(mainController.getView(), 960, 640);
        scene.getStylesheets().add(getClass().getResource("com/gabfusi/spaguetti/client/resources/app.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle("Spaguetti Uestern");
        stage.show();

        SocketClient socketService = null;
        MessageSender messageSender = null;
        ModelService modelService = null;

        // try to connect to server
        try {

            MessageReceiver messageReceiver = new MessageReceiver();
            socketService = new SocketClient(InetAddress.getLocalHost(), 8081, messageReceiver);
            messageSender = new MessageSender(socketService);

            // create Game
            Game game = new Game();
            modelService = new ModelService(messageSender, messageReceiver, game);

            factory.setModelService(modelService);

            // TODO check if user is logged in

            // if user isn't already subscribed to a round, show login
            mainController.showLogin();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }
}