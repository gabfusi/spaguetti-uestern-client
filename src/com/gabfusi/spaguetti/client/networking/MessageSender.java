package com.gabfusi.spaguetti.client.networking;

import com.gabfusi.spaguetti.client.model.Team;
import com.gabfusi.spaguetti.client.model.User;
import com.gabfusi.spaguetti.client.networking.socket.SocketClient;
import org.json.JSONObject;

import java.util.UUID;

public class MessageSender {

    private SocketClient socket;

    public MessageSender(SocketClient socket) {
        this.socket = socket;
    }

    /**
     * Perform a login request
     *
     * @param nickname
     * @param team
     */
    public void login(String nickname, Team team) {
        // subscribe a player to next round

        Message message = new Message(MessageActions.LOGIN);
        JSONObject payload = new JSONObject();
        payload.put("nickname", nickname);
        payload.put("team", team);
        message.setPayload(payload);

        // create User singleton
        User.getInstance().setNickname(nickname);
        User.getInstance().setTeam(team);

        this.sendMessage(message);
    }

    /**
     * never used
     */
    public void requestGameStatus() {
        Message message = new Message(MessageActions.GAME_STATUS);
        message.setUserId(User.getInstance().getId());
        this.sendMessage(message);
    }

    /**
     * Perform a player move request
     *
     * @param playerId
     * @param cityId
     */
    public void playerMove(UUID playerId, UUID cityId) {
        // perform a player move to cityId

        Message message = new Message(MessageActions.PLAYER_MOVE);
        JSONObject payload = new JSONObject();
        payload.put("cityId", cityId);
        payload.put("playerId", playerId);
        message.setPayload(payload);

        this.sendMessage(message);
    }

    /**
     * update player fight status
     *
     * @param playerId
     * @param answerId
     */
    public void fightPerformed(UUID playerId, int answerId) {

        Message message = new Message(MessageActions.FIGHT_PERFORMED);
        JSONObject payload = new JSONObject();
        payload.put("answerId", answerId);
        payload.put("playerId", playerId);
        message.setPayload(payload);

        this.sendMessage(message);

    }

    /**
     * @param message
     */
    private void sendMessage(Message message) {
        // convert Message to json
        String json = message.toJson();

        // return messages to connected client
        socket.send(json);
    }

}
