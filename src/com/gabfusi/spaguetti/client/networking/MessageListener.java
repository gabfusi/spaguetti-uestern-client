package com.gabfusi.spaguetti.client.networking;

public interface MessageListener {
    void onSocketMessage(Message message);
}