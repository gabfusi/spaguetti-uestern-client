package com.gabfusi.spaguetti.client.networking.socket;

/**
 * Handler interface
 * onReceive is triggered when the router receive a new message
 */
public interface MessageHandler {
    public void onReceive(SocketClient socket, String message);
}