package com.gabfusi.spaguetti.client.networking.socket;

import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Client networking
 */
public class SocketClient {
    private Socket socket;
    private MessageHandler handler;

    public SocketClient(InetAddress ip, int port, MessageHandler handler) {
        try {
            this.socket = new Socket(ip, port);
            this.handler = handler;
            listen();
        } catch (IOException e) {
            // server is down
            Alert alert = new Alert(Alert.AlertType.ERROR, "Connection to server refused.\nPlease check your internet connection.");
            alert.setHeaderText("Connection error");
            alert.showAndWait();
            Platform.exit();
        }
    }

    /**
     *
     */
    private void listen() {

        final Thread listenThread = new Thread(() -> {

            try {

                boolean isListening = true;
                BufferedReader reader;
                String message;

                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                while (isListening) {

                    message = reader.readLine();

                    if (message != null) {
                        handler.onReceive(this, message);
                    } else {
                        System.out.println("Seems that server shout down the connection...");
                        isListening = false;
                        close();
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
                // connection reset, server is down
            }
        });

        listenThread.setDaemon(true); // automatically shut down the thread on application exit
        listenThread.start();

    }

    /**
     *
     * @param message
     */
    public void send(String message) {
        PrintWriter writer;
        try {
            if (socket.isConnected()) {
                writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
                writer.println(message);
                System.out.println("Socket.send: " + message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method blocks
     *
     * @return string
     */
    public String readLine() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Close networking connection
     */
    private void close() {
        try {
            if (socket != null && !socket.isClosed())
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}