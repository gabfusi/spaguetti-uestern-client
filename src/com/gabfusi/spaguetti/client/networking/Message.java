package com.gabfusi.spaguetti.client.networking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class Message {
    private UUID userId = null;
    private MessageActions action;
    private JSONObject payload;

    /**
     *
     * @param action
     */
    Message(MessageActions action) {
        this.action = action;
        this.payload = new JSONObject();
    }

    /**
     *
     * @return
     */
    public UUID getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     */
    void setUserId(UUID userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     */
    public MessageActions getAction() {
        return action;
    }

    /**
     *
     * @return
     */
    public JSONObject getPayload() {
        return payload;
    }

    /**
     *
     * @param payload
     */
    void setPayload(JSONObject payload) {
        this.payload = payload;
    }

    /**
     *
     * @return
     */
    String toJson() {
        JSONObject source = new JSONObject();
        if (this.userId != null) {
            source.put("userId", this.userId.toString());
        } else {
            source.put("userId", "anonymous");
        }
        source.put("action", this.action);
        source.put("payload", this.payload);
        return source.toString();
    }

    /**
     * Factory
     * @param json
     * @return
     * @throws JSONException
     */
    static Message fromJson(String json) throws JSONException {

        JSONObject source = new JSONObject(json);

        if (!source.has("action")) {
            throw new JSONException("Received unrecognized message, no action specified.");
        }

        if (!source.has("payload")) {
            throw new JSONException("Received unrecognized message, no payload specified.");
        }

        MessageActions action = MessageActions.valueOf(source.getString("action"));
        JSONObject payload = source.getJSONObject("payload");

        Message message = new Message(action);
        message.setPayload(payload);

        return message;
    }
}
