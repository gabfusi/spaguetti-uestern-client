package com.gabfusi.spaguetti.client.service;

import com.gabfusi.spaguetti.client.controller.MainController;
import com.gabfusi.spaguetti.client.model.Game;
import com.gabfusi.spaguetti.client.networking.MessageReceiver;
import com.gabfusi.spaguetti.client.networking.MessageSender;

public class ModelService
{
    private MessageSender messageSender;
    private MessageReceiver messageReceiver;
    private Game game;

    public ModelService(MessageSender messageSender, MessageReceiver messageReceiver, Game game) {
        this.messageReceiver = messageReceiver;
        this.messageSender = messageSender;
        this.game = game;
    }

    public MessageSender getMessageSender() {
        return messageSender;
    }

    public MessageReceiver getMessageReceiver() {
        return messageReceiver;
    }

    public Game getGame() { return game; }
}