package com.gabfusi.spaguetti.client.graph;

public enum CellType {

    RECTANGLE,
    TRIANGLE,
    IMAGE,
    LABEL
    ;

}