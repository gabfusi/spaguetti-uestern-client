package com.gabfusi.spaguetti.client.graph;

public abstract class Layout {

    public abstract void execute();

}