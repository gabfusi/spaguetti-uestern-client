package com.gabfusi.spaguetti.client.graph;

import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;

import java.util.List;
import java.util.Random;

public class CircleLayout extends Layout {

    Graph graph;


    public CircleLayout(Graph graph) {

        this.graph = graph;

    }

    public void execute() {

        List<Cell> cells = graph.getModel().getAllCells();
        int cellsNumber = cells.size();

        Bounds graphBounds = graph.getScrollPane().getBoundsInParent();

        double centerX = graphBounds.getWidth()/2;
        double centerY = graphBounds.getHeight()/2;
        double radius = centerX > centerY ?
                centerX-100 : centerY-100;

        double slice = 2 * Math.PI / cellsNumber;
        int i = 0;

        for (Cell cell : cells) {

            double angle = slice * i;
            double x = (int)(centerX + radius * Math.cos(angle));
            double y = (int)(centerY + radius * Math.sin(angle));

            cell.relocate(x, y);

            i++;
        }

    }

}