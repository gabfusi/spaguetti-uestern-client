package com.gabfusi.spaguetti.client.graph;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.input.MouseEvent;

public class Graph {

    private Model model;
    private Group canvas;
    private ZoomableScrollPane scrollPane;
    private EventHandler<MouseEvent> onNodeClickHandler;

    // MouseGestures mouseGestures;

    /**
     * the pane wrapper is necessary or else the scrollpane would always align
     * the top-most and left-most child to the top and left eg when you drag the
     * top child down, the entire scrollpane would move down
     */
    CellLayer cellLayer;

    public Graph() {

        this.model = new Model();

        canvas = new Group();
        cellLayer = new CellLayer();

        canvas.getChildren().add(cellLayer);

        // mouseGestures = new MouseGestures(this);

        scrollPane = new ZoomableScrollPane(canvas);

        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);

    }

    public ScrollPane getScrollPane() {
        return this.scrollPane;
    }

    public void fitBounds() {
        this.scrollPane.zoomToFit(false);
    }

    public Pane getCellLayer() {
        return this.cellLayer;
    }

    public Model getModel() {
        return model;
    }

    public void beginUpdate() {
        canvas.getChildren().remove(this.cellLayer);
        this.cellLayer = new CellLayer();
        canvas.getChildren().add(this.cellLayer);

    }

    public void endUpdate() {

        // add components to graph pane
        getCellLayer().getChildren().addAll(model.getAddedEdges());
        getCellLayer().getChildren().addAll(model.getAddedCells());

        // remove components from graph pane
        getCellLayer().getChildren().removeAll(model.getRemovedCells());
        getCellLayer().getChildren().removeAll(model.getRemovedEdges());

        enableClickHandler();

        // every cell must have a parent, if it doesn't, then the graphParent is
        // the parent
        getModel().attachOrphansToGraphParent(model.getAddedCells());

        // remove reference to graphParent
        getModel().disconnectFromGraphParent(model.getRemovedCells());

        // merge added & removed cells with all cells
        getModel().merge();

    }

    public double getScale() {
        return this.scrollPane.getScaleValue();
    }


    public void setOnNodeClickHandler(EventHandler<MouseEvent> handler) {
        this.onNodeClickHandler = handler;
    }

    private EventHandler<MouseEvent> getOnNodeClickHandler() {
        return this.onNodeClickHandler;
    }

    public void enableClickHandler() {

        // enable click on cells
        for (Cell cell : model.getAddedCells()) {
            cell.setOnMouseClicked(getOnNodeClickHandler());
        }

    }

    public void disableClickHandler() {

        // disable click on cells
        for (Cell cell : model.getAddedCells()) {
            System.out.println("Disabling mouseclick on cell " + cell.getCellId());
            cell.setOnMouseClicked(null);
        }

    }
}