package com.gabfusi.spaguetti.client.graph.cells;

import com.gabfusi.spaguetti.client.graph.Cell;
import javafx.scene.control.Label;

public class LabelCell extends Cell {

    String label;

    public LabelCell(String id, String label) {
        super(id);

        this.label = label;
        Label view = new Label(label);

        setView(view);

    }

}