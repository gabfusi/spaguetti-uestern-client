package com.gabfusi.spaguetti.client.graph.cells;

import com.gabfusi.spaguetti.client.graph.Cell;
import com.gabfusi.spaguetti.client.model.Player;
import com.gabfusi.spaguetti.client.model.Team;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.awt.*;
import java.util.Collection;
import java.util.Hashtable;
import java.util.UUID;


public class CityCell extends Cell {

    String image;
    String label;
    HBox playersContainer;
    Collection<Player> players;

    public CityCell(String id, String _label, String _image, Collection<Player> _players, boolean selected) {
        super(id);

        this.image = _image;
        this.label = _label;
        this.players = _players;

        VBox view = new VBox();
        view.getStyleClass().add("graph-city");

        // city label
        Label label = new Label(_label);

        if(selected) {
            view.setId("current-city");
            label.setId("current-city-label");
        }

        view.getChildren().add(label);

        // image
        try{

            ImageView image = new ImageView(_image);
            image.setFitWidth(100);
            image.setFitHeight(80);
            view.getChildren().add(image);

        } catch(Exception e) {
            // invalid image
        }

        // players hbox
        playersContainer = new HBox();

        for(Player p : players) {
            addPlayer(p);
        }

        view.getChildren().add(playersContainer);

        setView(view);

    }

    public void addPlayer(Player player) {

        ImageView image;

        if(player.getTeam().equals(Team.WHITE)) {
            image = new ImageView("com/gabfusi/spaguetti/client/resources/bianco.jpg");
        } else {
            image = new ImageView("com/gabfusi/spaguetti/client/resources/nero.jpg");
        }

        image.setFitWidth(24);
        image.setFitHeight(24);

        String t = player.getTeam().toString().substring(0, 1);
        image.getStyleClass().addAll("graph-city-player", "graph-city-player-" + t);
        playersContainer.getChildren().add(image);

    }

}