package com.gabfusi.spaguetti.client.graph.cells;

import com.gabfusi.spaguetti.client.graph.Cell;
import javafx.scene.image.ImageView;


public class ImageCell extends Cell {

    public ImageCell(String id, String image) {
        super(id);

        ImageView view = new ImageView(image);
        view.setFitWidth(100);
        view.setFitHeight(80);

        setView(view);

    }

}