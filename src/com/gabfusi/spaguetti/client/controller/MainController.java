package com.gabfusi.spaguetti.client.controller;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import com.gabfusi.spaguetti.client.service.ModelService;

import eu.hansolo.enzo.notification.Notification;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;

public class MainController implements Initializable {
    @FXML
    private Parent root;
    @FXML
    private BorderPane contentArea;
    @FXML
    private ListView logList;

    private ModelService modelService;
    private LoginController loginController;
    private MatchmakingController matchmakingController;
    private GameController gameController;
    private ResultsController resultsController;

    private ArrayList<String> logs = new ArrayList<>();
    private ListProperty<String> listProperty = new SimpleListProperty<>();

    public Parent getView() {
        return root;
    }

    public void setModelService(ModelService service) {
        modelService = service;
    }

    public void setLoginController(LoginController presenter) {
        loginController = presenter;
    }

    public void setMatchmakingController(MatchmakingController presenter) {
        matchmakingController = presenter;
    }

    public void setGameController(GameController presenter) {
        gameController = presenter;
    }

    public void setResultsController(ResultsController presenter) {
        resultsController = presenter;
    }

    /**
     * Show login view
     */
    public void showLogin() {
        contentArea.setCenter(loginController.getView());
    }

    /**
     * Show matchmaking view
     */
    public void showMatchmaking() {
        contentArea.setCenter(matchmakingController.getView());
    }

    /**
     * Show game view
     */
    public void showGame() {
        // javafx updates must resides on FX application thread.
        Platform.runLater(() -> {
            contentArea.setCenter(gameController.getView());
        });
    }

    /**
     * Show results view
     */
    public void showResults() {
        // javafx updates must resides on FX application thread.
        Platform.runLater(() -> {
            contentArea.setCenter(resultsController.getView());
        });
    }


    /**
     * Show a glowl-like notification
     * @param message
     * @param type
     */
    public void showNotification(String message, String type) {

        Notification.Notifier.INSTANCE.setAlwaysOnTop(true);

        if (type.equals("warning")) {
            Notification.Notifier.INSTANCE.notifyWarning("", message);
        } else if (type.equals("error")) {
            Notification.Notifier.INSTANCE.notifyError("", message);
        } else if (type.equals("success")) {
            Notification.Notifier.INSTANCE.notifySuccess("", message);
        } else {
            Notification.Notifier.INSTANCE.notifyInfo("", message);
        }

    }

    /**
     * Show an error dialog
     * @param message
     * @param header
     */
    public void showErrorDialog(String message, String header) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message);
        alert.setHeaderText(header);
        alert.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logList.itemsProperty().bind(listProperty);
        listProperty.set(FXCollections.observableArrayList(logs));
    }

    public void addLog(String message) {
        String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
        logs.add(0, "[" + timeStamp + "] " + message);
        Platform.runLater(() -> listProperty.set(FXCollections.observableArrayList(logs)));
    }
}