package com.gabfusi.spaguetti.client.controller;

import com.gabfusi.spaguetti.client.graph.*;
import com.gabfusi.spaguetti.client.graph.cells.CityCell;
import com.gabfusi.spaguetti.client.model.City;
import com.gabfusi.spaguetti.client.model.Player;
import com.gabfusi.spaguetti.client.model.Team;
import com.gabfusi.spaguetti.client.model.User;
import com.gabfusi.spaguetti.client.networking.Message;
import com.gabfusi.spaguetti.client.networking.MessageListener;
import com.gabfusi.spaguetti.client.service.ModelService;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.*;

public class GameController implements Initializable, MessageListener {
    @FXML
    private Node root;
    @FXML
    private ListView playersList;
    @FXML
    private BorderPane graphPane;
    @FXML
    private Label infoLabel;
    @FXML
    private Label currentAmmoLabel;

    private ArrayList<String> playersWaiting = new ArrayList<>();
    private ListProperty<String> listProperty = new SimpleListProperty<>();
    private Graph graph;

    private MainController mainController;
    private ModelService modelService;

    public Node getView() {
        return root;
    }

    public void setMainController(MainController presenter) {
        mainController = presenter;
    }

    public void setModelService(ModelService service) {
        modelService = service;
    }

    /**
     * Executed as soon as the view loads
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        playersList.itemsProperty().bind(listProperty);
        listProperty.set(FXCollections.observableArrayList(playersWaiting));
        //startCountdown();

        graph = new Graph();
        graph.setOnNodeClickHandler(onCityClickHandler);
        graphPane.setCenter(graph.getScrollPane());
    }

    /**
     * Executed on each socket message received
     *
     * @param message
     */
    @Override
    public void onSocketMessage(Message message) {

        //System.out.println("onSocketMessage " + message.getAction() + " " + message.getPayload().toString());

        JSONObject payload = message.getPayload();

        switch (message.getAction()) {

            case PLAYER_EXITED:
                modelService.getGame().unsubscribePlayer(UUID.fromString(payload.getString("id")));
                modelService.getGame().updatePlayersPosition();
                refreshPlayerList();
                Platform.runLater(this::drawWorldGraph);
                break;

            case ROUND_START:
                updateGameStatus(payload);
                Platform.runLater(() -> updateUserAmmo());
                break;

            case PLAYER_ROUND_START:

                System.out.println("Player round start -> " + payload.toString());

                Player player = Player.fromJson(payload);
                modelService.getGame().setCurrentPlayer(player);

                Platform.runLater(() -> {
                    updateTurnStatus(player);
                    if (modelService.getGame().isUserTurn()) {
                        mainController.showNotification("Hurry up! It's your turn!", "info");
                        mainController.addLog("It's your turn!");
                    } else {
                        mainController.addLog("It's " + player.getNickname() + " turn.");
                    }
                });

                break;

            case AMMO_FOUND:

                int collectedAmmo = payload.getInt("ammo") - User.getInstance().getAmmo();
                User.getInstance().setAmmo(payload.getInt("ammo"));

                Platform.runLater(() -> {
                    updateUserAmmo();
                    mainController.showNotification("Congrats! You collected " + collectedAmmo + " ammo!", "info");
                    mainController.addLog("You collected " + collectedAmmo + " ammo!");
                });

                break;

            case AMMO_UPDATED:

                int updatedAmmo = payload.getInt("ammo");
                User.getInstance().setAmmo(updatedAmmo);

                Platform.runLater(() -> {
                    updateUserAmmo();
                    mainController.addLog("Your current ammo number is now " + User.getInstance().getAmmo());
                });

                break;

            case ENEMY_FOUND:
                Platform.runLater(() -> mainController.showNotification("You found an enemy!", "info"));
                mainController.addLog("You found an enemy. :S");
                break;

            case FIGHT_START:
                mainController.addLog("A fight has started, please answer this question...");
                Platform.runLater(() -> showQuestionDialog(payload));
                break;

            case FIGHT_END:

                String status = payload.getString("winnerTeam");

                if (status.equals("draw")) {

                    Platform.runLater(() -> mainController.showNotification("No team win, both scored the same points!", "info"));
                    mainController.addLog("The fight ended with a draw, players ammo remains untouched.");

                } else {

                    Team winnerTeam = Team.valueOf(status);

                    if (winnerTeam.equals(User.getInstance().getTeam())) {
                        Platform.runLater(() -> mainController.showNotification("Your team won!", "success"));
                        mainController.addLog("Your team won the fight!");
                    } else {
                        Platform.runLater(() -> mainController.showNotification(winnerTeam + " Team won!", "warning"));
                        mainController.addLog("Your team lost the fight. :(");
                    }
                }

                updateGameStatus(payload);

                break;

            case UGLY_ENCOUNTERED:
                int updatedUserAmmo = payload.getInt("ammo");
                User.getInstance().setAmmo(updatedUserAmmo);

                Platform.runLater(() -> {
                    mainController.showNotification("The Ugly stole you 1 ammo!", "warning");
                    updateUserAmmo();
                    mainController.addLog("The Ugly stole you 1 ammo!");
                });

                break;

            case PLAYER_ROUND_END:
                System.out.println("Player round end -> " + payload.toString());
                updateGameStatus(payload);
                break;

            case PLAYER_MOVE:
                updateGameStatus(payload);
                break;

            case ROUND_END:
                // Go to results view
                mainController.showResults();
                mainController.addLog("Game ended!");
                break;
        }

    }

    /**
     * @param messagePayload
     */
    private void updateGameStatus(JSONObject messagePayload) {

        // players
        // update whole player list, some changes may happened server side.
        JSONArray players = messagePayload.getJSONArray("players");
        modelService.getGame().setPlayers(players);

        // world
        JSONObject world = messagePayload.getJSONObject("world");
        JSONArray cities = world.getJSONArray("cities");
        modelService.getGame().setCities(cities);

        // assign players to cities
        modelService.getGame().updatePlayersPosition();

        refreshPlayerList();
        Platform.runLater(this::drawWorldGraph);

    }

    /**
     * Refresh players listview
     */
    private void refreshPlayerList() {
        // quick & rough: clear players ListView and populate again
        playersWaiting.clear();

        for (Player player : modelService.getGame().getPlayers()) {
            String name;
            if(User.getInstance().getId().equals(player.getId())) {
                name = "YOU";
            } else {
                name = player.getNickname();
            }
            playersWaiting.add(name + " (" + player.getTeam() + " Team)");
        }

        // as http://stackoverflow.com/questions/16730981/javafx-thread-to-update-a-listview
        Platform.runLater(() -> listProperty.set(FXCollections.observableArrayList(playersWaiting)));
    }

    /**
     * Draw world graph
     */
    private void drawWorldGraph() {

        ArrayList<JSONObject> edges = new ArrayList<>();

        Model model = graph.getModel();
        model.clear();

        graph.beginUpdate();

        // add nodes (cities)
        for (City city : modelService.getGame().getCities()) {

            boolean isCurrentPlayerCity = city.getId().equals(User.getInstance().getCurrentCityId());

            model.addCityCell(city.getId().toString(), city.getName(), city.getImage(), city.getPlayers(), isCurrentPlayerCity);

            for (UUID connectedCityId : city.getConnections()) {
                JSONObject edge = new JSONObject();
                edge.put("cityId", city.getId().toString());
                edge.put("connectionId", connectedCityId.toString());
                edges.add(edge);
            }
        }

        // add edges (paths)
        for (JSONObject edge : edges) {
            model.addEdge(edge.getString("cityId"), edge.getString("connectionId"));
        }

        graph.endUpdate();

        Layout layout = new CircleLayout(graph);

        Platform.runLater(() -> {
            layout.execute();
            //graph.fitBounds();
        });
    }

    /**
     * On node (city) click
     */
    private EventHandler<MouseEvent> onCityClickHandler = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent event) {

            CityCell node = (CityCell) event.getSource();

            City currentCity = modelService.getGame().getCity(User.getInstance().getCurrentCityId());
            City desiredCity = modelService.getGame().getCity(UUID.fromString(node.getCellId()));

            if (!currentCity.isConnectedTo(desiredCity)) {
                System.out.println("Cannot move to selected city!");
                return;
            }

            if (!modelService.getGame().isUserTurn()) {
                System.out.println("It isn't your turn!");
                return;
            }

            System.out.println("User want to travel to " + desiredCity.getName());

            modelService.getMessageSender().playerMove(User.getInstance().getId(), desiredCity.getId());

        }
    };

    /**
     * Update GUI on player turn change
     *
     * @param currentPlayer
     */
    private void updateTurnStatus(Player currentPlayer) {

        if (modelService.getGame().isUserTurn()) {
            infoLabel.setText("It's your turn!");
            //graph.enableClickHandler();
        } else {
            infoLabel.setText("It's " + currentPlayer.getNickname() + " turn!");
            //graph.disableClickHandler();
        }
    }

    /**
     * Update GUI when User ammo changes
     */
    private void updateUserAmmo() {
        currentAmmoLabel.setText("Ammos: " + User.getInstance().getAmmo());
    }

    /**
     * Show fight question dialog
     *
     * @param qa
     */
    private void showQuestionDialog(JSONObject qa) {

        String question = qa.getString("question");
        JSONArray answers = qa.getJSONArray("answers");
        List<String> choices = new ArrayList<>();

        for (int i = 0; i < answers.length(); i++) {
            String answer = answers.getString(i);
            choices.add(answer);
        }

        // see http://code.makery.ch/blog/javafx-dialogs-official/
        ChoiceDialog<String> dialog = new ChoiceDialog<>(null, choices);
        dialog.setTitle("Fight with your knowledge!");
        dialog.setHeaderText("Fight with your knowledge!");
        dialog.setContentText(question);

        Optional<String> result = dialog.showAndWait();
        result.ifPresent(s -> {

            int answerIndex = choices.indexOf(s);

            modelService.getMessageSender().fightPerformed(User.getInstance().getId(), answerIndex);

            System.out.println("Your choice: " + s);
        });

    }

}