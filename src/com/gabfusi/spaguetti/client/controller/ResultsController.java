package com.gabfusi.spaguetti.client.controller;


import com.gabfusi.spaguetti.client.model.Player;
import com.gabfusi.spaguetti.client.model.Team;
import com.gabfusi.spaguetti.client.model.User;
import com.gabfusi.spaguetti.client.networking.Message;
import com.gabfusi.spaguetti.client.networking.MessageListener;
import com.gabfusi.spaguetti.client.service.ModelService;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.*;

public class ResultsController implements Initializable, MessageListener {
    @FXML
    private Node root;
    @FXML
    public ListView<String> whiteTeamList;
    @FXML
    public ListView<String> blackTeamList;
    @FXML
    public Label resultsInfoLabel;
    @FXML
    public Label winnerTeamLabel;


    private ArrayList<String> whiteTeamPlayers = new ArrayList<>();
    private ListProperty<String> whiteTeamListProperty = new SimpleListProperty<>();
    private ArrayList<String> blackTeamPlayers = new ArrayList<>();
    private ListProperty<String> blackTeamListProperty = new SimpleListProperty<>();
    private MainController mainController;
    private ModelService modelService;

    public Node getView() {
        return root;
    }

    public void setMainController(MainController presenter) {
        mainController = presenter;
    }

    public void setModelService(ModelService service) {
        modelService = service;
    }


    /**
     * Executed as soon as the view loads
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        whiteTeamList.itemsProperty().bind(whiteTeamListProperty);
        blackTeamList.itemsProperty().bind(blackTeamListProperty);
    }


    /**
     * Refresh players listview
     */
    private void refreshPlayerLists() {
        // quick & rough: clear players ListView and populate again
        whiteTeamPlayers.clear();
        blackTeamPlayers.clear();

        // order players by ammo
        Collection<Player> playerList = modelService.getGame().getPlayers();
        Player[] players = playerList.toArray(new Player[playerList.size()]);
        Comparator<Player> c = Comparator.comparing(Player::getAmmo);
        Arrays.sort(players, c);

        for (Player player : players) {

            if (player.getTeam().equals(Team.WHITE)) {
                whiteTeamPlayers.add(player.getNickname() + " (" + player.getAmmo() + " ammo)");
            } else {
                blackTeamPlayers.add(player.getNickname() + " (" + player.getAmmo() + " ammo)");
            }

        }

        // as http://stackoverflow.com/questions/16730981/javafx-thread-to-update-a-listview
        Platform.runLater(() -> {
            whiteTeamListProperty.set(FXCollections.observableArrayList(whiteTeamPlayers));
            blackTeamListProperty.set(FXCollections.observableArrayList(blackTeamPlayers));
        });
    }


    /**
     * Executed on each socket message received
     *
     * @param message
     */
    @Override
    public void onSocketMessage(Message message) {

        System.out.println("Results: onSocketMessage " + message.getAction() + " " + message.getPayload().toString());

        JSONObject payload = message.getPayload();

        switch (message.getAction()) {

            // on round end
            case ROUND_END:

                JSONArray players = payload.getJSONArray("players");
                modelService.getGame().setPlayers(players);

                JSONObject results = payload.getJSONObject("results");
                boolean isDraw = results.getBoolean("isDraw");
                int whiteTeamAmmo = results.getInt(Team.WHITE.toString());
                int blackTeamAmmo = results.getInt(Team.BLACK.toString());

                Platform.runLater(() -> {

                    if (isDraw) {

                        winnerTeamLabel.setText("DRAW.");
                        resultsInfoLabel.setText("Your team and the opposing team collected the same amount of ammo (" + whiteTeamAmmo + ").");

                    } else {

                        Team winnerTeam = Team.valueOf(results.getString("winnerTeam"));
                        Team loserTeam = winnerTeam.equals(Team.WHITE) ? Team.BLACK : Team.WHITE;
                        int winnerTeamAmmo = whiteTeamAmmo > blackTeamAmmo ? whiteTeamAmmo : blackTeamAmmo;
                        int loserTeamAmmo = whiteTeamAmmo > blackTeamAmmo ? blackTeamAmmo : whiteTeamAmmo;

                        if (winnerTeam.equals(User.getInstance().getTeam())) {

                            winnerTeamLabel.setText("Your Team Won!");
                            resultsInfoLabel.setText("Your team won " + winnerTeamAmmo + " - " + loserTeamAmmo + " against " + loserTeam + " Team");

                        } else {
                            winnerTeamLabel.setText("Your Team Lose.");
                            resultsInfoLabel.setText("Your team lose " + loserTeamAmmo + " - " + winnerTeamAmmo + " against " + winnerTeam + " Team");
                        }

                    }

                });

                refreshPlayerLists();

                break;
        }

    }
}