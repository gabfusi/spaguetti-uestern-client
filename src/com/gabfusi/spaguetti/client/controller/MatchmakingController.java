package com.gabfusi.spaguetti.client.controller;

import com.gabfusi.spaguetti.client.model.Player;
import com.gabfusi.spaguetti.client.model.User;
import com.gabfusi.spaguetti.client.networking.Message;
import com.gabfusi.spaguetti.client.networking.MessageListener;
import com.gabfusi.spaguetti.client.service.ModelService;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.util.Duration;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class MatchmakingController implements Initializable, MessageListener {
    @FXML
    private Node root;
    @FXML
    private ListView playersList;
    @FXML
    private Label countdownLabel;

    private long countdownTime;
    private Timeline timeline;
    protected ArrayList<String> playersWaiting = new ArrayList<>();
    protected ListProperty<String> listProperty = new SimpleListProperty<>();

    private MainController mainController;
    private ModelService modelService;

    public Node getView() {
        return root;
    }

    public void setMainController(MainController presenter) {
        mainController = presenter;
    }

    public void setModelService(ModelService service) {
        modelService = service;
    }

    /**
     * Executed as soon as the view loads
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        playersList.itemsProperty().bind(listProperty);
        listProperty.set(FXCollections.observableArrayList(playersWaiting));
        startCountdown();

    }

    /**
     * Executed on each socket message received
     *
     * @param message
     */
    @Override
    public void onSocketMessage(Message message) {

        long gameTime;
        long nextRoundTime;
        JSONObject payload = message.getPayload();

        switch (message.getAction()) {

            // on current player login
            case LOGIN_ACK:

                // update players list
                JSONArray players = payload.getJSONArray("players");

                // set players to game
                for (int i = 0; i < players.length(); i++) {
                    JSONObject player = players.getJSONObject(i);
                    modelService.getGame().subscribePlayer(Player.fromJson(player));
                }

                // quick & rough: clear players ListView and populate again
                refreshPlayerList();

                // countdown
                gameTime = payload.getLong("time");
                nextRoundTime = payload.getLong("next_round_at");
                adjustCountdown(gameTime, nextRoundTime);

                mainController.addLog("You joined the game with nickname \"" + User.getInstance().getNickname() + "\" and Team " + User.getInstance().getTeam());

                break;

            case GAME_TIME:
                gameTime = payload.getLong("time");
                nextRoundTime = payload.getLong("next_round_at");
                adjustCountdown(gameTime, nextRoundTime);
                break;

            case PLAYER_JOINED:
                Player player = Player.fromJson(payload);
                modelService.getGame().subscribePlayer(player);
                mainController.addLog(player.getNickname() + " joined the game.");
                refreshPlayerList();
                break;

            case PLAYER_EXITED:
                UUID playerId = UUID.fromString(payload.getString("id"));
                mainController.addLog(modelService.getGame().getPlayer(playerId).getNickname() + " exited the game.");
                modelService.getGame().unsubscribePlayer(playerId);
                refreshPlayerList();
                break;

            case ROUND_START:
                timeline.stop();
                mainController.showGame();
                mainController.addLog("A Game round has started!");
                break;
        }

    }

    /**
     * Refresh players listview
     */
    private void refreshPlayerList() {
        // quick & rough: clear players ListView and populate again
        playersWaiting.clear();

        for (Player player : modelService.getGame().getPlayers()) {
            playersWaiting.add(player.getNickname() + " (" + player.getTeam() + " Team)");
        }

        // as http://stackoverflow.com/questions/16730981/javafx-thread-to-update-a-listview
        Platform.runLater(() -> listProperty.set(FXCollections.observableArrayList(playersWaiting)));
    }

    /**
     * Start countdown timer
     */
    private void startCountdown() {

        timeline = new Timeline(new KeyFrame(Duration.millis(1000), (ae) -> {
            if (countdownTime > 0) {
                countdownTime -= 1000;
                countdownLabel.setText(formatCountdownString(countdownTime));
            }
        }));

        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    /**
     * Adjust countdown with server time
     *
     * @param gameTime
     * @param nextRoundAt
     */
    private void adjustCountdown(long gameTime, long nextRoundAt) {
        this.countdownTime = nextRoundAt - gameTime;
    }

    /**
     * Format countdown string
     *
     * @param millis
     * @return
     */
    private String formatCountdownString(long millis) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

}