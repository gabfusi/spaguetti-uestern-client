package com.gabfusi.spaguetti.client.controller;


import java.net.URL;
import java.util.ResourceBundle;

import com.gabfusi.spaguetti.client.model.Team;
import com.gabfusi.spaguetti.client.networking.Message;
import com.gabfusi.spaguetti.client.networking.MessageListener;
import javafx.application.Platform;
import javafx.scene.control.*;
import com.gabfusi.spaguetti.client.service.ModelService;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import org.json.JSONObject;

public class LoginController implements Initializable, MessageListener {
    @FXML
    private Node root;
    @FXML
    public Button loginButton;
    @FXML
    public TextField nicknameField;
    @FXML
    public ToggleGroup teamRadio;

    private MainController mainController;
    private ModelService modelService;

    public Node getView() {
        return root;
    }

    public void setMainController(MainController presenter) {
        mainController = presenter;
    }

    public void setModelService(ModelService service) {
        modelService = service;
    }

    /**
     *
     */
    public void loginSubmit() {

        String playerNickname = nicknameField.getText();
        RadioButton teamField = (RadioButton) teamRadio.getSelectedToggle();
        Team playerTeam;

        if (!validate(playerNickname, teamField)) {
            Platform.runLater(() -> mainController.showErrorDialog("Please insert a valid username and choose a team!", "Login error"));
            return;
        }

        if (teamField.getText().equals("Black Team")) {
            playerTeam = Team.BLACK;
        } else {
            playerTeam = Team.WHITE;
        }

        // perform login
        modelService.getMessageSender().login(playerNickname, playerTeam);
    }


    /**
     * Executed on each socket message received
     *
     * @param message
     */
    @Override
    public void onSocketMessage(Message message) {

        //System.out.println("Login: onSocketMessage " + message.getAction() + " " + message.getPayload().toString());

        JSONObject payload = message.getPayload();

        switch (message.getAction()) {

            // on login error
            case ERROR:
                String text = payload.getString("message");
                Platform.runLater(() -> {
                    mainController.showLogin();
                    mainController.showErrorDialog(text, "Login error");
                });
                break;

            // on login successful
            case LOGIN_ACK:

                // if login ok show matchmaking
                Platform.runLater(() -> mainController.showMatchmaking());

                break;
        }

    }

    /**
     *
     * @param nickname
     * @param team
     * @return
     */
    private boolean validate(String nickname, RadioButton team) {
        return !nickname.isEmpty() && team != null;
    }

    /**
     * Executed as soon as the view loads
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

//	/**
//	 * @author zonski
//   * @url http://www.zenjava.com/2011/12/11/javafx-and-mvp-a-smorgasbord-of-design-patterns/
//	 *
//	 */
//    public void setContact(final Long id)
//    {
//        this.contactId = contactId;
//        firstNameField.setText("");
//        lastNameField.setText("");
//        final Task<Contact> loadTask = new Task<Contact>()
//        {
//            protected Contact call() throws Exception
//            {
//                return contactService.getContact(contactId);
//            }
//        };
//
//        loadTask.stateProperty().addListener(new ChangeListener<Worker.State>()
//        {
//            public void changed(ObservableValue<? extends Worker.State> source, Worker.State oldState, Worker.State newState)
//            {
//                if (newState.equals(Worker.State.SUCCEEDED))
//                {
//                    Contact contact = loadTask.getValue();
//                    firstNameField.setText(contact.getFirstName());
//                    lastNameField.setText(contact.getLastName());
//                }
//            }
//        });
//
//        new Thread(loadTask).start();
//    }
}