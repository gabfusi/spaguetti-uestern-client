package com.gabfusi.spaguetti.client.model;

public enum Team {
    WHITE,
    BLACK,
    UGLY;
}