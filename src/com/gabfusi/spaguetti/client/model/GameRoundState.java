package com.gabfusi.spaguetti.client.model;

public enum GameRoundState {
    MATCHMAKING,
    IDLE,
    STARTED,
    ENDED;
}
