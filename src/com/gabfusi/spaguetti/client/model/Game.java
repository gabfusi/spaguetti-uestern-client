package com.gabfusi.spaguetti.client.model;

import org.json.JSONArray;

import java.util.Collection;
import java.util.Hashtable;
import java.util.UUID;

public class Game {

    private Hashtable<UUID, Player> playerList;
    private Hashtable<UUID, City> cityList;
    private Player currentPlayer = null;

    private final Object lockPlayers = new Object();
    private final Object lockCities = new Object();

    public Game() {
        cityList = new Hashtable<>();
        playerList = new Hashtable<>();
    }

    // players

    public Player getPlayer(UUID playerId) {
        return playerList.get(playerId);
    }

    public void subscribePlayer(Player player) {
        playerList.putIfAbsent(player.getId(), player);
    }

    public void unsubscribePlayer(UUID playerId) {
        playerList.remove(playerId);
    }

    public void clearPlayers() {
        playerList.clear();
    }

    public Collection<Player> getPlayers() {
        return playerList.values();
    }

    public void setPlayers(JSONArray players) {

        synchronized (lockPlayers) {
            playerList.clear();
            for (int i = 0; i < players.length(); i++) {
                Player player = Player.fromJson(players.getJSONObject(i));
                subscribePlayer(player);

                // update user
                if (User.getInstance().getId().equals(player.getId())) {
                    User.getInstance().setCurrentCityId(player.getCurrentCityId());
                    User.getInstance().setTeam(player.getTeam());
                    //User.getInstance().setAmmo(player.getAmmo());
                }
            }
        }

    }

    // cities

    public Collection<City> getCities() {
        return cityList.values();
    }

    public void setCities(JSONArray cities) {
        synchronized (lockCities) {
            cityList.clear();
            for (int i = 0; i < cities.length(); i++) {
                City city = City.fromJson(cities.getJSONObject(i));
                addCity(city);
            }

        }
    }

    public City getCity(UUID cityId) {
        return cityList.get(cityId);
    }

    public void addCity(City city) {
        cityList.putIfAbsent(city.getId(), city);
    }

    public void updateCity(UUID cityId, City city) {
        synchronized (lockCities) {
            cityList.put(cityId, city);
        }
    }

    public void updatePlayersPosition() {

        for (City city : getCities()) {

            city.clearPlayers();

            for (Player player : getPlayers()) {

                if (player.getCurrentCityId().equals(city.getId())) {
                    city.addPlayer(player);
                }

            }

        }

    }

    public void setCurrentPlayer(Player player) {
        currentPlayer = player;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean isUserTurn() {
        return currentPlayer != null && User.getInstance().getId() != null && currentPlayer.getId().equals(User.getInstance().getId());
    }
}
