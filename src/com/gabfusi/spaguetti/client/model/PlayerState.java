package com.gabfusi.spaguetti.client.model;

public enum PlayerState {
    WAITING,
    PLAYING,
    FIGHTING;
}